

import pandas as pd

def test_data_conversion():
    def process_data(df, columns_select):
        # Assuming df is the DataFrame before conversion
        data_loaded_sub_part = df[columns_select]
        # Insert more DF operations if needed
        return data_loaded_sub_part

    # Load the raw data (before conversion)
    raw_data_path = 'Downloads/choice_rtt/sourcedata/sub-01/ses-post/01_post_crtt_exp_2024-02-02_09h43.24.388.csv'  # Update this path
    raw_data_df = pd.read_csv(raw_data_path, delimiter=',')

    # Columns to select and any other processing details
    columns_select = ['participant_id', 'age', 'left-handed', 'Do you like this session?', 'session', 'TargetImage', 'keyboard_response.corr', 'trialRespTimes']

    # Process the raw data
    processed_data_df = process_data(raw_data_df, columns_select)

    # Load the expected data (after conversion) for comparison
    expected_data_path = 'Downloads/choice_rtt/sub-01/ses-post/beh/sub-01_ses-post_task-ChoiceRTT_beh.tsv'  # Update this path
    expected_data_df = pd.read_csv(expected_data_path, delimiter='\t')

    # Assertions
    assert list(processed_data_df.columns) == list(expected_data_df.columns), "Columns do not match"
    assert processed_data_df.shape == expected_data_df.shape, "DataFrame shapes do not match"
    
    # Compare the first row as dicts
    processed_first_row = processed_data_df.iloc[0].to_dict()
    expected_first_row = expected_data_df.iloc[0].to_dict()
    
    for key in processed_first_row:
        if isinstance(processed_first_row[key], float):
            assert abs(processed_first_row[key] - expected_first_row[key]) < 1e-5, f"Row values do not match for column {key}"
        else:
            assert processed_first_row[key] == expected_first_row[key], f"Row values do not match for column {key}"
